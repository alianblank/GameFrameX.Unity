# 介绍：

GameFrameX for Unity是GameFrameX综合解决方案的重要组成部分，专为Unity客户端设计。它融合了众多实用的功能组件，形成了一个强大的模块库，旨在为游戏的前端开发、后端服务及管理界面提供一个一体化平台。这个解决方案促进了不同系统间的无缝协作，实现了高效的游戏开发和运维流程。

> 正在开发中...

# `注意`!!! `注意` !!! `注意` !!!

开发期,随时可能变化结构。

# 客户端

## `Unity` 集成功能

|        组件名称        |         介绍          |   来源   | 链接地址                                                                 |
|:------------------:|:-------------------:|:------:|:---------------------------------------------------------------------|
|   GameFramework    |        客户端框架        | GitHub | https://github.com/AlianBlank/GameFrameX                             |
|      YooAsset      |      定制的资源包管理       | GitHub | https://github.com/AlianBlank/com.tuyoogame.yooasset                 |
|      UniTask       |  异步Await/Async的实现   | GitHub | https://github.com/AlianBlank/com.cysharp.unitask                    |
|    FairyGUI UI     |         编辑器         | GitHub | https://github.com/AlianBlank/com.fairygui.unity                     |
|      ProtoBuf      |     数据序列化和通讯协议      | GitHub | https://github.com/AlianBlank/com.google.protobuf                    |
|    MessagePack     |     高效的二进制序列化库      | GitHub | https://github.com/AlianBlank/com.neuecc.messagepack                 |
|     HybridCLR      |         热更新         | GitHub | https://github.com/focus-creative-games/hybridclr                    |
|        XLua        |         热更新         | GitHub | https://github.com/AlianBlank/com.tencent.xlua                       |
|   GameAnalytics    |      游戏数据分析和统计      | GitHub | 还没上传                                                                 |
|       Sentry       |      错误追踪和性能监控      | GitHub | https://github.com/AlianBlank/io.sentry.unity                        |
|      LitJson       |  JSON序列化工具（马三修改版本）  | GitHub | https://github.com/AlianBlank/com.xincger.litjson                    |
|     logViewer      |        日志查看器        | GitHub | https://github.com/AlianBlank/com.sharelib.logviewer                 |
|      DoTween       |       强大的动画插件       | GitHub | https://github.com/AlianBlank/com.demigiant.dotween                  |
|     Animancer      |    高度灵活的动画状态机插件     | GitHub | 还没上传                                                                 |
|      BestHTTP      |     全面的HTTP协议实现     | GitHub | https://github.com/AlianBlank/com.benedicht.besthttp                 |
| OperationClipBoard |    实现剪贴板数据的设置与获取    | GitHub | https://github.com/AlianBlank/com.alianblank.blankoperationclipboard |
|     GetChannel     |  渠道获取及集成基础的渠道获取方式   | GitHub | https://github.com/AlianBlank/com.alianblank.blankgetchannel         |
|     ReadAssets     | 直接读取Android只读目录下的文件 | GitHub | https://github.com/AlianBlank/com.alianblank.readassets              |
|   FindReference2   |     强大的资源引用查找插件     | GitHub | https://github.com/AlianBlank/com.vietlabs.fr2                       |

# 交流方式(建议。需求。BUG)

QQ群：467608841

# Doc

文档地址 : https://www.yuque.com/alianblank/gameframex

# 免责声明

所有插件均来自互联网.请各位使用时自行付费.如果以上插件涉及侵权.请发email.本人将移除.谢谢

# LICENSE

Apache License 2
